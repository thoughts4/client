// For storing backend urls. If we define a common class for all urls then
// we dont have to make changes all over the code if there is a change related
// to url.

class Paths {
  static const String thoughts = "/thoughts";
  static const String thought = "/thought/%s";
  static const String thoughtTags = "/thoughts/%s/tags";
  static const String thoughtRelationshipsTags =
      "/thoughts/%s/relationships/tags";
  static const String tags = "/tags";
  static const String tag = "/tags/%s";
  static const String tagThoughts = "/tags/%s/thoughts";
  static const String tagRelationshipsThoughts =
      "/tags/%s/relationships/thoughts";
}
