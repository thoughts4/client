// Common utilities.
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:sprintf/sprintf.dart';

import 'package:thoughts_client/constants.dart' as constants;
import 'package:thoughts_client/utils/local_store.dart';

Uri formUri(String path,
    {List<String>? ids,
    Map<String, dynamic>? filter,
    List<String>? sort,
    List<String>? include,
    Map<String, dynamic>? fields,
    Map<String, String>? page}) {
  Map<String, String> queryParams = {constants.tenant: LocalStore.getTenant()};

  // Replace `id` placeholder with actual values.
  if (ids != null) {
    path = sprintf(path, ids);
  }
  // todo: complete other queryParam options.

  if (include != null) {
    queryParams[constants.include] = include.join(",");
  }

  if (dotenv.env[constants.mode] != constants.development) {
    return Uri.https(
        dotenv.env[constants.serverUrl] ??
            (throw ArgumentError(constants.serverUrl)),
        path,
        queryParams);
  }
  return Uri.http(
      dotenv.env[constants.serverUrl] ??
          (throw ArgumentError(constants.serverUrl)),
      path,
      queryParams);
}
