// To store local information like username, password, tenant-id etc.

import 'package:thoughts_client/constants.dart' as constants;

class LocalStore {
  // todo: Hardcoded tenant value
  //      for testing purpose only
  //      remove it after implementing
  //      signin & signup.
  static Map<String, String> _store = {constants.tenant: "1049601"};

  static String getTenant() {
    return _store[constants.tenant] ??
        (throw ArgumentError("${constants.tenant} is required"));
  }
}
