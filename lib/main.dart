import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:provider/provider.dart';

import 'package:thoughts_client/widgets/top_bar.dart';
import 'package:thoughts_client/widgets/bottom_bar.dart';
import 'package:thoughts_client/widgets/menu_drawer.dart';
import 'package:thoughts_client/screens/thought_landing.dart';

void main() async {
  await dotenv.load(fileName: "lib/.env");
  runApp(App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => AppState(),
      child: MaterialApp(
        title: 'thoughts',
        theme: ThemeData(
          useMaterial3: true,
          colorSchemeSeed: const Color(0xff6750a4),
        ),
        home: HomePage(),
      ),
    );
  }
}

class AppState extends ChangeNotifier {
  var current = WordPair.random();

  void getNext() {
    current = WordPair.random();
    notifyListeners();
  }

  var favorites = <WordPair>{};

  void toggleFavorite() {
    if (favorites.contains(current)) {
      favorites.remove(current);
    } else {
      favorites.add(current);
    }
    notifyListeners();
  }
}

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var selectedIndex = 0;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    Widget page;
    switch (selectedIndex) {
      case 0:
        page = GeneratorPage();
        break;
      case 1:
        page = FavoritesPage();
        break;
      default:
        throw UnimplementedError('no widget for $selectedIndex');
    }

    return LayoutBuilder(builder: (context, constraints) {
      return Scaffold(
          key: scaffoldKey,
          appBar: TopBar(),
          bottomNavigationBar: BottomBar(),
          drawer: MenuDrawer(),
          // body: ThoughtLandingPage(),
          body: Column(
            children: [
              ThoughtLandingPage(),
            ],
          )
          // body: Row(
          //   children: [
          //     ThoughtLandingPage()
          // SafeArea(
          //   child: NavigationRail(
          //     extended: constraints.maxWidth >= 600,
          //     destinations: [
          //       NavigationRailDestination(
          //         icon: Icon(Icons.home),
          //         label: Text('Home'),
          //       ),
          //       NavigationRailDestination(
          //         icon: Icon(Icons.favorite),
          //         label: Text('Favorites'),
          //       ),
          //     ],
          //     selectedIndex: selectedIndex,
          //     onDestinationSelected: (value) {
          //       setState(() {
          //         selectedIndex = value;
          //       });
          //     },
          //   ),
          // ),
          // Expanded(
          //   child: SingleChildScrollView(
          //     child: Container(
          //         color: Theme.of(context).colorScheme.primaryContainer,
          //         // child: page,
          //         child: ThoughtCard(
          //             "123",
          //             "1997-07-16T19:20:30.45+01:00",
          //             "Founded in 1909 by eighteen football players from Dortmund, the football team is part of a large membership-based sports club with more than 145,000 members,[6] making Borussia Dortmund the second largest sports club by membership in Germany. The club has active departments in other sports, namely in women's handball. Since 1974, Dortmund have played their home games at Westfalenstadion; the stadium is the largest in Germany, and Dortmund has the highest average attendance of any association football club in the world.[7] Borussia Dortmund's colours are black and yellow, giving the club its nickname die Schwarzgelben.[8][9] They hold a long-standing rivalry with Ruhr neighbours Schalke 04, with whom they contest the Revierderby. They also contest Der Klassiker with Bayern Munich. In terms of Deloitte's annual Football Money League, Dortmund was in 2021 ranked as the second richest sports club in Germany, and the 12th richest football team in the world.10 Moreover, under the directorship",
          //             "happy_and_loved", [
          //           "happy",
          //           "coding",
          //           "hooyah",
          //           "happy",
          //           "coding",
          //           "hooyah"
          //         ])),
          //   ),
          // ),
          //   ],
          // ),
          );
    });
  }
}

class GeneratorPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var appState = context.watch<AppState>();
    var pair = appState.current;

    IconData icon;
    if (appState.favorites.contains(pair)) {
      icon = Icons.favorite;
    } else {
      icon = Icons.favorite_border;
    }

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          BigCard(pair: pair),
          SizedBox(height: 10),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              ElevatedButton.icon(
                onPressed: () {
                  appState.toggleFavorite();
                },
                icon: Icon(icon),
                label: Text('Like'),
              ),
              SizedBox(width: 10),
              ElevatedButton(
                onPressed: () {
                  appState.getNext();
                },
                child: Text('Next'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class BigCard extends StatelessWidget {
  const BigCard({
    Key? key,
    required this.pair,
  }) : super(key: key);

  final WordPair pair;

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var style = theme.textTheme.displayMedium!.copyWith(
      color: theme.colorScheme.onPrimary,
    );

    return Card(
      color: theme.colorScheme.primary,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Text(
          pair.asLowerCase,
          style: style,
          semanticsLabel: pair.asPascalCase,
        ),
      ),
    );
  }
}

class FavoritesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var appState = context.watch<AppState>();

    if (appState.favorites.isEmpty) {
      return Center(
        child: Text('No favorites yet.'),
      );
    }

    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.all(20),
          child: Text('You have '
              '${appState.favorites.length} favorites:'),
        ),
        for (var pair in appState.favorites)
          ListTile(
            leading: Icon(Icons.favorite),
            title: Text(pair.asLowerCase),
          ),
      ],
      // children: appState.favorites
      //     .map((pair) => ListTile(
      //           title: Text(pair.asLowerCase),
      //           textColor: Theme.of(context).colorScheme.secondary,
      //         ))
      //     .toList(),
    );
  }
}
