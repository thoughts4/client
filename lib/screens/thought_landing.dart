// Landing page for thought.

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:thoughts_client/widgets/thought_card.dart';
import 'package:thoughts_client/paths.dart';
import 'package:thoughts_client/utils/common.dart' as common_utils;
import 'package:thoughts_client/constants.dart' as constants;

Future<dynamic> _getThoughts(Uri uri) async {
  final http.Response response = await http.get(uri);
  return jsonDecode(response.body);
}

class ThoughtLandingPage extends StatelessWidget {
  // Group tags from include based on tag's id.
  Map<String, dynamic> _groupTags(List<dynamic>? included) {
    Map<String, dynamic> groupedTags = {};
    if (included == null) {
      return groupedTags;
    }

    for (Map<String, dynamic> record in included) {
      if (record[constants.type] == constants.tag) {
        groupedTags[record[constants.id]] = record;
      }
    }
    return groupedTags;
  }

  List<String> _getTagNames(
      Map<String, dynamic> groupedTags, List<String> tagIds) {
    List<String> tagNames = [];
    for (String tagId in tagIds) {
      if (groupedTags.containsKey(tagId)) {
        tagNames.add(groupedTags[tagId][constants.attributes]["name"]);
      }
    }
    return tagNames;
  }

  List<String> _formTagIds(List<dynamic> relationships) {
    List<String> tagIds = [];
    for (Map<String, dynamic> record in relationships) {
      tagIds.add(record[constants.id]);
    }
    return tagIds;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _getThoughts(
            common_utils.formUri(Paths.thoughts, include: ["tags"])),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return CircularProgressIndicator();
          }

          if (snapshot.hasData) {
            Map<String, dynamic> groupedTags =
                _groupTags(snapshot.data[constants.included]);

            List<ThoughtCard> children = [];
            for (Map<String, dynamic> thoughtRecord
                in snapshot.data[constants.data]) {
              List<String> tagIds = [];
              if (thoughtRecord.containsKey(constants.relationships) &&
                  thoughtRecord[constants.relationships].containsKey("tags")) {
                tagIds = _formTagIds(thoughtRecord[constants.relationships]
                    ["tags"][constants.data]);
              }

              children.add(
                ThoughtCard(
                    thoughtRecord[constants.id],
                    thoughtRecord[constants.attributes]["lastModifiedTime"],
                    thoughtRecord[constants.attributes]["description"],
                    thoughtRecord[constants.attributes]["label"],
                    _getTagNames(groupedTags, tagIds)),
              );
            }

            return Expanded(
              child: SingleChildScrollView(
                child: Container(
                    color: Theme.of(context).colorScheme.primaryContainer,
                    child: Column(
                      children: children,
                    )),
              ),
            );
          }

          if (snapshot.hasError) {
            return Text('500 - error!');
          }
          return Text("");
        });
  }
}
