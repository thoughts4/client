// Bottom navigation bar.
// Bottom navigation bar consists of 3 items,
// 1. Home
//    On clicking on `home` icon, main thoughts page will be displayed with most
//    recent thoughts.
// 2. Search
//    On clicking `search` button, a search bar will appear in the top, replacing
//    logo. Search will be based on the content of either `thought` or `tag` w.r.t
//    from which page the user clicks search.
// 3. Filter
//    On clicking `filter` button, a filterable list will be displayed similar to
//    search at the top replacing logo. User has to select the filterable option
//    and type/choose filter value. Filter options will be showed based on the
//    current page from where user clicks the filter icon.

import 'package:flutter/material.dart';

class BottomBar extends StatelessWidget {
  final int _homeIndex = 0;
  final int _searchIndex = 1;
  final int _filterIndex = 2;

  @override
  Widget build(BuildContext context) {
    int selectedIndex = 0;

    void onItemTapped(int index) {
      print("tapped on, $index");
    }

    return BottomNavigationBar(
      showSelectedLabels: false,
      showUnselectedLabels: false,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.search),
          label: 'Search',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.filter_alt_outlined),
          label: 'Filter',
        ),
      ],
      currentIndex: selectedIndex,
      onTap: onItemTapped,
      // elevation: 5
    );
  }
}
