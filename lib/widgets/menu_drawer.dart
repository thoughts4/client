// Menu-drawer contains more info about the app like settings, support.
// The drawer contains 4 parts.
// 1. User info
//    User info section has 2 parts.
//    1. User's profile picture/avatar.
//    2. Joined date.
// 2. Tags
//    To display tags section.
// 3. Settings
// 4. Support

import 'package:flutter/material.dart';

class MenuDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.primaryContainer,
              ),
              child: Column(
                children: [
                  CircleAvatar(
                    radius: 40,
                    backgroundColor: Colors.black,
                    child: CircleAvatar(
                      backgroundImage:
                          AssetImage('assets/logos/jikan_desu_icon.png'),
                      backgroundColor: Theme.of(context).colorScheme.background,
                      radius: 38,
                    ),
                  ),
                  Text('Joined on'),
                  Text('12 Mar 2023')
                ],
              )),
          ListTile(
            leading: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(
                "#",
                style: TextStyle(fontSize: 30),
              ),
            ),
            title: TextButton(
              onPressed: () {},
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text('Tags'),
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: TextButton(
              onPressed: () {},
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text('Settings'),
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.support),
            title: TextButton(
              onPressed: () {},
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text('Support'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
