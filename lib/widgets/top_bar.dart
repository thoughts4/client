//  Top bar component for the application.
//
//  1. Left most side contains a circular display picture.
//    1.1. On clicking, drawer from left with menu.
//  2. At middle there will be app's logo.
//    2.1 On clicking control will go to default landing page.
//
// Note:  Currently top app bar is made as a stateless widget but in future
//        may have to convert it to stateFul for usecase like changing
//        display-pic/avatar

import 'package:flutter/material.dart';

class TopBar extends StatelessWidget with PreferredSizeWidget {
  @override
  final Size preferredSize;

  TopBar({
    Key? key,
  })  : preferredSize = Size.fromHeight(50.0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      shadowColor: Theme.of(context).shadowColor,
      // Avatar button
      leading: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FloatingActionButton.small(
          onPressed: () => Scaffold.of(context).openDrawer(),
          child: Icon(
            Icons.person,
            color: Colors.black,
          ),
        ),
      ),
      // logo button
      title: SizedBox(
        width: 95,
        child: IconButton(
            onPressed: () => {},
            icon: Image.asset('assets/logos/jikan_desu_icon.png')),
      ),
    );
  }
}
