// A card structure to display thought.
// A thought-card will have 5 parts.
//    1. Date and time in human readable format upto minutes.
//    2. Labels for the thought (emojis like loved, happy etc.)
//    3. Content.
//    4. A line separated section to display tag names.
//      NOTE: In future a clickable action may needed for tag-name, on clicking
//            it will go to search all thoughts with the tag-name.
//    5. A delete button at the top right corner. Display a conformation message
//        before deleting the thought.

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:google_fonts/google_fonts.dart';

class ThoughtCard extends StatelessWidget {
  final String _id;
  final String _lastModifiedTime;
  final String _description;
  final String? _label;
  final List<String>? _tagNames;

  ThoughtCard(this._id, this._lastModifiedTime, this._description, this._label,
      this._tagNames);

  // Convert thought's labels to emojis.
  Widget _buildLabel(String? label) {
    Widget emojiWidget = Text("");
    TextStyle style = TextStyle(fontSize: 25);
    if (label == "sad") {
      emojiWidget = Text('😞', style: style);
    } else if (label == "depressed") {
      emojiWidget = Text('😣', style: style);
    } else if (label == "sad_and_depressed") {
      emojiWidget = Text('😞 😣', style: style);
    } else if (label == "happy") {
      emojiWidget = Text('😁', style: style);
    } else if (label == "loved") {
      emojiWidget = Text('😍', style: style);
    } else if (label == "happy_and_loved") {
      emojiWidget = Text('😁 😍', style: style);
    }

    return emojiWidget;
  }

  // Convert ISO8601 format to more human readable form.
  String _iso8601ToNormalDate(String date) {
    DateTime date_ = DateTime.parse(date);

    String dayAbbr = DateFormat('E').format(date_).toString();
    String dayNumber = DateFormat('d').format(date_).toString();
    String monthYear = DateFormat('yMMMM').format(date_).toString();
    String hourMinutes = DateFormat('jm').format(date_).toString();

    return '$dayAbbr, $dayNumber $monthYear $hourMinutes';
  }

  // Build tag chips.
  List<Widget> _buildTags(List<String>? tagNames, BuildContext context) {
    List<Widget> tagChips = [];
    if (tagNames == null || tagNames.isEmpty) {
      return tagChips;
    }
    for (var i = 0; i < tagNames.length; i++) {
      tagChips.add(Chip(
        label: Text(tagNames[i]),
        backgroundColor: Theme.of(context).colorScheme.primaryContainer,
      ));
    }
    return tagChips;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      margin: EdgeInsets.all(12),
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: Theme.of(context).colorScheme.outline,
        ),
        borderRadius: const BorderRadius.all(Radius.circular(12)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SizedBox(
          child: IntrinsicHeight(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      _iso8601ToNormalDate(_lastModifiedTime),
                      style: TextStyle(fontSize: 18),
                    ),
                    Spacer(),
                    IconButton(
                        onPressed: (() => {}), icon: Icon(Icons.delete_forever))
                  ],
                ),
                Row(children: <Widget>[_buildLabel(_label)]),
                Row(children: <Widget>[
                  Expanded(
                      child: Text(_description,
                          style: GoogleFonts.indieFlower(fontSize: 18)))
                ]),
                Divider(
                  thickness: 1.2,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Wrap(
                      spacing: 5, children: _buildTags(_tagNames, context)),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
