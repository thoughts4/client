// To define constant values.

const String serverUrl = "SERVER_URL";
const String mode = "MODE";
const String development = "development";

const String tenant = "tenant";
const String include = "include";

const String tag = "tag";

const String attributes = "attributes";
const String relationships = "relationships";
const String type = "type";
const String id = "id";
const String data = "data";
const String included = "included";
