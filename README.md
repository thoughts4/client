# Thoughts client

Client part of thoughts project.

## Description

Client part of thoughts project is built with [flutter](https://docs.flutter.dev/). The primary reason to choose flutter is fast development and cross platform support. Client will be using server apis to handle user data.

## Installation

Follow this [page](https://docs.flutter.dev/get-started/install) to install flutter on your device

## Usage

Run the `main.dart` file to run the app.

## Roadmap

Havent planned yet. After completing all preliminary work for a simple app, all advanced features has to be planned.

## Contributing

Follow the below `branch` and `commit` format for all contributions.

### Prefixes

Use the below prefixes in branch name and commit message.

1. feat - For a new feature.
2. fix - For fixing an issue.
3. refactor - For improving the code base without having a big impact.
4. doc - For documentations.
5. ci - For ci/cd changes.

### Branch naming

* Branch name **MUST** begin with a predefined [prefix](#prefixes) followed by a `/`(slash).
* Branch name **SHOULD** be small(Dont describe everything in branch name).
* Words in branch name **MUST** be separated with `-`(hyphen).

> eg: `feat/entity-definition`, `fix/create-service-for-user`

### Commit naming
* Commit name **MUST** begin with a predefined [prefix](#prefixes) followed by a `: `(collon and space).
* Commit message **MUST** be specific and **MUST** justify changes in the commit.
* Use multiline commit, if there are more things to describe.
* Words in commit **MUST** be separated with ` `(space).

> eg: `feat: server linting`, `fix: raise exception from create service for user`

### Precommit

Precommit is not added right now, will be added in future. Only commits passing all pre-commit hooks will be allowed to merge.

## Authors and acknowledgment

No one so far.

## License

Yet to be done.

## Project status

Its an ongoing project in the beginning status.